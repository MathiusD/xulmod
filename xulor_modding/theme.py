import requests


class Theme:

    def __init__(self, textures: list, pixmaps: list, colors: list, themeElements: list, **kwargs):
        self.textures = textures
        self.pixmaps = pixmaps
        self.colors = colors
        self.themeElements = themeElements

    @staticmethod
    def fetchLastTheme():
        response = requests.get(
            "https://wakfu.cdn.ankama.com/gamedata/theme/theme.json")
        if response.status_code == 200:
            return response.json()
        else:
            raise Exception("Error on fetch of latest theme")
