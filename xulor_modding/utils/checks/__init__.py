from .bounds import checkInBounds
from .defined import checkIsDefined
from .rgb import checkIsRGB
from .type import checkIsGoodType

__all__ = [
    "checkInBounds",
    "checkIsDefined",
    "checkIsRGB",
    "checkIsGoodType"
]
