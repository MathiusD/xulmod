from .base import Base
from .coordinate import Coordinate
from .texture import Texture
from .size import Size


class Pixmap(Base):

    def __init__(self, id: str, texture: Texture, position: str, coordinate: Coordinate, size: Size,
                 flipHorizontally: bool, flipVertically: bool, since: str, **kwargs):
        self.texture = texture
        self.position = position
        self.coordinate = coordinate
        self.size = size
        self.flipHorizontally = flipHorizontally
        self.flipVertically = flipVertically
        super().__init__(id, since)
