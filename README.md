# Xulor Modding CLI

[![Pipeline Status](https://gitlab.com/MathiusD/xulor-modding-cli/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/xulor-modding-cli/-/pipelines)
[![Coverage Report](https://gitlab.com/MathiusD/xulor-modding-cli/badges/master/coverage.svg)](https://mathiusd.gitlab.io/xulor-modding-cli/reports/coverage)
[Mutation Report](https://mathiusd.gitlab.io/xulor-modding-cli/reports/mutation)
