from unittest import TestCase

from parameterized import parameterized
from xulor_modding.utils.checks.type import checkIsGoodType


class TestType(TestCase):

    @parameterized.expand([
        (12, int),
        (12, int, {'attribute': 'example'}),
        (12, int, {'other': 'example'}),
        ('ex', str),
        ('ex', str, {'attribute': 'example'}),
        ('ex', str, {'other': 'example'}),
        (None, int, {}, AttributeError("value must be defined")),
        (None, int, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, int, {'other': 'example'}, AttributeError(
            "value must be defined")),
        ("foo", int, {}, AttributeError("value must be an integer")),
        ("foo", int, {'attribute': 'example'}, AttributeError(
            "example must be an integer")),
        ("foo", int, {'other': 'example'}, AttributeError(
            "value must be an integer")),
        (24, str, {}, AttributeError("value must be an integer")),
        (24, str, {'attribute': 'example'}, AttributeError(
            "example must be an integer")),
        (24, str, {'other': 'example'}, AttributeError(
            "value must be an integer")),
    ])
    def testCheckRGB(self, value, typeExpected, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkIsGoodType(value, typeExpected, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkIsGoodType(value, typeExpected, **kwargs)
