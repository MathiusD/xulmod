from unittest import TestCase

from parameterized import parameterized
from xulor_modding.utils.checks.rgb import checkIsRGB


class TestRGB(TestCase):

    @parameterized.expand([
        (12,),
        (12, {'attribute': 'example'}),
        (12, {'other': 'example'}),
        (0,),
        (0, {'attribute': 'example'}),
        (0, {'other': 'example'}),
        (255,),
        (255, {'attribute': 'example'}),
        (255, {'other': 'example'}),
        (None, {}, AttributeError("value must be defined")),
        (None, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, {'other': 'example'}, AttributeError(
            "value must be defined")),
        ("foo", {}, AttributeError("value must be an integer")),
        ("foo", {'attribute': 'example'}, AttributeError(
            "example must be an integer")),
        ("foo", {'other': 'example'}, AttributeError(
            "value must be an integer")),
        (256, {}, AttributeError("value must be in following bounds (0-255)")),
        (256, {'attribute': 'example'}, AttributeError(
            "example must be in following bounds (0-255)")),
        (256, {'other': 'example'}, AttributeError(
            "value must be in following bounds (0-255)")),
        (-1, {}, AttributeError("value must be in following bounds (0-255)")),
        (-1, {'attribute': 'example'}, AttributeError(
            "example must be in following bounds (0-255)")),
        (-1, {'other': 'example'}, AttributeError(
            "value must be in following bounds (0-255)")),
    ])
    def testCheckRGB(self, value, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkIsRGB(value, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkIsRGB(value, **kwargs)
