from unittest import TestCase

from parameterized import parameterized
from xulor_modding.size import Size


class TestSize(TestCase):

    @parameterized.expand([
        (1, 5),
        (-25, 65),
        (None, None)
    ])
    def testInit(self, width: int, height: int):
        size = Size(width, height)
        self.assertEqual(size.width, width)
        self.assertEqual(size.height, height)
